@extends('layouts.main')

@section('title', 'Daftar Pertanyaan')

@section('content')
    <div class="d-flex">
        <a href="questions/create" class="btn btn-primary">Tambah Pertanyaan</a>
    </div>
    <div class="row">
        <div class="col">
            @include('questions.alert')
            <ul class="list-group mt-2">
                @foreach( $questions as $question )
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-auto">
                            {{ $loop->iteration }}
                        </div>
                        <div class="col">
                            <div class="d-flex justify-content-between">
                                <h5>{{ $question->judul }}</h5>
                                <small>{{ $question->created_at }}</small>
                            </div>
                            <div class="d-flex justify-content-between">
                                <p class="mb-1">{{ $question->isi }}</p>
                                <div>
                                    <a href="questions/{{ $question->id }}/show" class="badge badge-primary badge-pill">Detail</a>
                                    <a href="questions/{{ $question->id }}/edit" class="badge badge-success badge-pill">Edit</a>
                                    <a href="questions/{{ $question->id }}/delete" class="badge badge-danger badge-pill">Hapus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection