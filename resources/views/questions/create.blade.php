@extends('layouts.main')

@section('title', 'Tambah Pertanyaan')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <form action="/questions/store" method="post">
        @csrf
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul" id="judul">
            @error('judul')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi">Isi pertanyaan</label>
            <input type="text" class="form-control @error('isi') is-invalid @enderror" name="isi" id="isi">
            @error('isi')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambahkan</button>
        </form>
    </div>
</div>
@endsection