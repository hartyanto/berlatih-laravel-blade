@if(session('create-success'))
    <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
        {!! session('create-success') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(session('edit-success'))
    <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
        {!! session('edit-success') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(session('delete-success'))
    <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
        {!! session('delete-success') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif