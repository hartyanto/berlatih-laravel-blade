@extends('layouts.main')

@section('title', $question->judul)

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <h5 class="card-title">{{ $question->judul }}</h5>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal">
                        Hapus pertanyaan
                    </button>
                </div>
                <small class="text-muted">Created at {{ $question->created_at }}</small>
                <p class="card-text">{{ $question->isi }}</p>
            </div>
        </div>
    </div>
</div>
    

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Pertanyaan {{ $question->judul }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            Anda yakin ingin menghapus pertanyaan ini ?
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <a href="/questions/{{ $question->id }}/delete" class="btn btn-sm btn-danger">Hapus</a>
            </div>
        </div>
        </div>
    </div>
@endsection