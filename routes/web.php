<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::view('/', 'index');
Route::view('/data-tables', 'data');

Route::get('/questions', 'QuestionsController@index');
Route::get('/questions/{questions_id}/show', 'QuestionsController@show');

Route::get('/questions/create', 'QuestionsController@create');
Route::post('/questions/store', 'QuestionsController@store');

Route::get('/questions/{questions_id}/edit', 'QuestionsController@edit');
Route::put('/questions/{questions_id}', 'QuestionsController@update');

Route::get('/questions/{questions_id}/delete', 'QuestionsController@delete');
