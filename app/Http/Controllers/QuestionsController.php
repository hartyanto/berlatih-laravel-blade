<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionsController extends Controller
{
    public function index(Request $request)
    {
        $questions = DB::table('questions')->latest()->get();
        return view('questions.index', compact('questions'));
    }

    public function show($id)
    {
        $question = DB::table('questions')->where('id', $id)->first();
        return view('questions.show', compact('question'));
    }

    public function create()
    {
        return view('questions.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:questions',
            'isi' => 'required'
        ]);

        DB::table('questions')->insert([
            'judul' => $request->judul,
            'isi' => $request->isi
        ]);

        return redirect('/questions')->with('create-success', 'Pertanyaan <strong>berhasil</strong> ditambahkan.');
    }

    public function edit($id)
    {
        $question = DB::table('questions')->where('id', $id)->first();
        return view('questions.edit', compact('question'));
    }

    public function update($id, Request $request)
    {
        $judul = DB::table('questions')->where('id', $id)->value('judul');
        if ($request->judul == $judul) {
            $question = $request->validate([
                'judul' => 'required',
                'isi' => 'required'
            ]);
        } else {
            $question = $request->validate([
                'judul' => 'required|unique:questions',
                'isi' => 'required'
            ]);
        }

        DB::table('questions')->where('id', $id)->update([
            'judul' => $question['judul'],
            'isi' => $question['isi']
        ]);

        return redirect('/questions')->with('edit-success', 'Pertanyaan <strong>berhasil</strong> diubah.');
    }

    public function delete($id)
    {
        DB::table('questions')->where('id', $id)->delete();

        return redirect('/questions')->with('delete-success', 'Pertanyaan <strong>berhasil</strong> dihapus.');
    }
}
